Pod::Spec.new do |s|
  
  # Set Up Variables
  name       = 'NABCompiledLibraries'
  version    = '2.1.0'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCCFWK/objc-compiled-frameworks.git'
  source_dir = 'Pod'
  s.source   = {:git => git_url, :branch => "release/#{version}"}


  # Main Spec - Information
  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'Compiled frameworks for NAB\'s iOS, OSX apps.'
  s.homepage = git_url
  
  
  # Main Spec - Technicals
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.10'
  
  s.requires_arc = true
  
  
  # Cocoapods bugs:
  # - s.ios.default_subspec and osx doesn't work.
  # - s.osx.dependency also includes ios stuffs
  s.default_subspec = 'Dummy'
  
  
  
  # ----------- Subspecs ------------
  
  
  
  s.subspec 'Dummy' do |ss|
    # This is an empty subspec, serves merely as a dummy default subspec,
    # else ALL other subspecs will be installed.
  end
  
  
  
  s.subspec 'iOS' do |ss|
    
    # Dependencies
    ss.dependency 'NABCompiledFrameworks/iOS'
    ss.dependency s.name + '/HockeySDK+NAB'
    ss.dependency s.name + '/GoogleAnalytics+NAB'
    ss.dependency s.name + '/NABViewer'
    ss.dependency s.name + '/NABSignInViewController'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'
    
  end



  s.subspec 'HockeySDK+NAB' do |ss|
    
    # Platform
    ss.platform = :ios

    # Frameworks
    ss.frameworks =
      'Foundation'

    # Dependencies
    ss.dependency 'HockeySDK'
    ss.dependency 'NABCompiledFrameworks/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'GoogleAnalytics+NAB' do |ss|
    
    # Platform
    ss.platform = :ios

    # Dependencies
    ss.dependency 'GoogleAnalytics'
    ss.dependency 'NABCompiledFrameworks/NABLog'
    ss.dependency 'NABCompiledFrameworks/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABViewer' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'

    # Dependencies
    ss.dependency 'libextobjc'
    ss.dependency 'FBAudienceNetwork'
    ss.dependency 'JRSwizzle'
    ss.dependency 'mopub-ios-sdk'
    ss.dependency 'pop'
    ss.dependency 'POP+MCAnimate'
    ss.dependency 'QZConstraints'
    ss.dependency 'Reachability'
    ss.dependency 'NABCompiledFrameworks/Apple+NAB'
    ss.dependency 'NABCompiledFrameworks/NABUIFeedback'
    ss.dependency 'NABCompiledFrameworks/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'
      
    # Resources
    ss.resources =
      source_dir + '/' + ss.name + '/Resources/*.*'

  end
  
  
  
  s.subspec 'NABSignInViewController' do |ss|

    # Platform
    ss.platform = :ios

    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'

    # Dependencies
    ss.dependency 'libextobjc'
    ss.dependency 'NABCompiledFrameworks/Apple+NAB'
    ss.dependency 'NABCompiledFrameworks/UIKit+NAB'
    ss.dependency s.name + '/GoogleAnalytics+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

    # Resources
    ss.resources =
      source_dir + '/' + ss.name + '/Resources/*.*'

  end
  
  
  
end