Pod::Spec.new do |s|
  
  name       = 'NABCompiledFrameworks'
  version    = '1.0.8'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCCFWK/objc-compiled-frameworks.git'
  source_dir = 'Source'



  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'Objective-C compiled frameworks for NAB\'s iOS apps.'
  s.homepage = git_url
  
  s.platform = :ios
  s.ios.deployment_target = "7.0"
  
  
  
  s.ios.frameworks  =
    'AssetsLibrary',
    'AudioToolbox',
    'AVFoundation',
    'CFNetwork',
    'CoreData',
    'CoreGraphics',
    'CoreLocation',
    'CoreMedia',
    'CoreText',
    'Foundation',
    'ImageIO',
    'QuartzCore',
    'StoreKit',
    'SystemConfiguration',
    'UIKit'



  s.ios.dependency 'Facebook-iOS-SDK'
  s.ios.dependency 'GoogleAnalytics-iOS-SDK'
  s.ios.dependency 'instagram-ios-sdk'
  s.ios.dependency 'JSONKit-NoWarning'
  s.ios.dependency 'NSData+Base64'
  s.ios.dependency 'OAuthCore'
  s.ios.dependency 'Parse'
  s.ios.dependency 'Reachability'
  s.ios.dependency 'SBJson'
  
  s.ios.xcconfig = {
    'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Parse',
    'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
  }

  
  
  s.header_dir   = name
  s.header_mappings_dir = source_dir
  
  s.source       = {:git => git_url, :tag => version}
  s.source_files =
    source_dir,
    source_dir + '/*.{h,m}',
    source_dir + '/**/*.{h,m}'

  
  
  s.requires_arc = true
  
  non_arc_files =
    source_dir + '/ObjectiveFlickr/ObjectiveFlickr.{h,m}',
    source_dir + '/ObjectiveFlickr/OFUtilities.{h,m}',
    source_dir + '/ObjectiveFlickr/OFXMLMapper.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFWebAPIKit.h',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFHTTPRequest.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFSiteReachability.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/NSData+LFHTTPFormExtensions.{h,m}',
    source_dir + '/GusUtils/NSMutableDictionary+ImageMetadata.{h,m}',
    source_dir + '/NSAttributedString+Attributes/NSAttributedString+Attributes.{h,m}'
    
  s.exclude_files = non_arc_files
  
  s.subspec 'no-arc' do |sna|
    sna.requires_arc = false
    sna.source_files = non_arc_files
  end

end
