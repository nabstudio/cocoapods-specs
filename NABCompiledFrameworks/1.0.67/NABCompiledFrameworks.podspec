Pod::Spec.new do |s|
  
  # Set Up Variables
  name       = 'NABCompiledFrameworks'
  version    = '1.0.67'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCCFWK/objc-compiled-frameworks.git'
  source_dir = 'Source'
  s.source   = {:git => git_url, :tag => version} # Comment/Uncomment this for development/distribution mode.


  # Main Spec - Information
  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'Objective-C compiled frameworks for NAB\'s iOS apps.'
  s.homepage = git_url
  
  
  # Main Spec - Technicals
  s.ios.deployment_target = '7.0'
  s.osx.deployment_target = '10.8'
  
  s.requires_arc = true
  s.xcconfig = {
    'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
  }
  
  
  # Cocoapods bugs:
  # - s.ios.default_subspec and osx doesn't work.
  # - s.osx.dependency also includes ios stuffs
  s.default_subspec = 'iOS/Core'
  
  
  
  # ----------- Specs ------------
  
  
  
  s.subspec 'iOS' do |ss|
    
    
    # Platform
    #ss.platform = :ios - Don't put here! It is considered whole pod.
    
    
    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'
      
    
    # Subspecs
    ss.subspec 'Core' do |sss|
      
      # Platform
      sss.platform = :ios

      # Frameworks
      sss.frameworks =
        'AudioToolbox', # NABSoundManager
        'AVFoundation', # AVAssetExportSession+NAB
        'CoreData', # NABMagicalRecord
        'StoreKit' # MKStoreManager

      sss.ios.frameworks =
        'AssetsLibrary', # PhotoServices
        'MessageUI', # Email
        'UIKit'

      sss.ios.weak_frameworks =
        'Photos' # Only on iOS 8, PhotoServices

      # Dependencies
      sss.dependency 'JSONKit-NoWarning' # NABVersionUpdateController
      sss.dependency 'libextobjc' # All
      sss.dependency 'NSData+Base64' # MKStoreManager, NABNetworkKit
      sss.dependency 'pop' # NABSnappingCollectionView
      sss.dependency 'POP+MCAnimate' # NABSnappingCollectionView
      sss.dependency 'Reachability' # UIApplication+NABAppStore, NABNetworkKit

      sss.dependency 'Facebook-iOS-SDK' # NABFacebookManager
      sss.dependency 'GoogleAnalytics-iOS-SDK' # GAI+NAB
      sss.dependency 'instagram-ios-sdk' # NABInstagramManager
      sss.dependency 'Parse' # NABFacebookManager, NABSourceDataObject
#      sss.dependency 'ParseFacebookUtilsV4' # NABFacebookManager, NABSourceDataObject
      sss.dependency 'RBStoryboardLink' # NABTransition, NABUniversalStoryboardLink
      sss.dependency 'SFHFKeychainUtils' # MKStoreManager

      sss.dependency s.name + '/NABDefines'
      sss.dependency s.name + '/NABLog'
      sss.dependency s.name + '/NABFlickr'

      # Technicals
      sss.xcconfig = {
        'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Parse $(PODS_ROOT)/ParseFacebookUtils',
        'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
      }

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end
    
    
    ss.subspec 'AAPLFramework' do |sss|

      # Platform
      sss.platform = :ios

      # Dependencies
      sss.dependency s.name + '/NABLog'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/Source/**/*.{h,m}'

      sss.resources =
        source_dir + '/' + sss.name + '/Resources/*.*'

    end
    
    
    ss.subspec 'RGBCube' do |sss|

      # Platform
      sss.platform = :ios

      # Frameworks
      sss.frameworks =
        'CoreGraphics',
        'CoreImage',
        'Foundation',
        'UIKit'

      # Dependencies
      sss.dependency ss.name + '/Core'
      sss.dependency 'libextobjc'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end
    
    
    ss.subspec 'UICollectionViewFlowLayout+NAB' do |sss|

      # Platform
      sss.platform = :ios

      # Frameworks
      sss.frameworks =
        'Foundation',
        'UIKit'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end
    
    
    ss.subspec 'NABSignInViewController' do |sss|

      # Platform
      sss.platform = :ios

      # Frameworks
      sss.frameworks =
        'UIKit'

      # Dependencies
      sss.dependency ss.name + '/Core'
      sss.dependency 'libextobjc'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/Source/**/*.{h,m}'

      sss.resources =
        source_dir + '/' + sss.name + '/Resources/*.*'

    end
    
    
    ss.subspec 'NABPopulatingView' do |sss|

      # Platform
      sss.platform = :ios

      # Frameworks
      sss.frameworks =
        'UIKit'

      # Dependencies
      sss.dependency ss.name + '/Core'
      sss.dependency 'QZConstraints'
      sss.dependency 'libextobjc'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/Source/**/*.{h,m}'

      sss.resources =
        source_dir + '/' + sss.name + '/Resources/*.*'

    end
    
    
    ss.subspec 'NABUIFeedback' do |sss|

      # Platform
      sss.platform = :ios

      # Frameworks
      sss.frameworks =
        'Foundation',
        'UIKit'

      # Dependencies
      sss.dependency 'pop'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end
    
    
    ss.subspec 'NABLocalization' do |sss|

      # Platform
      sss.platform = :ios

      # Frameworks
      sss.frameworks =
        'Foundation'

      # Dependencies
      sss.dependency 'JRSwizzle'
      sss.dependency ss.name + '/Core'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end
    
      
  end
  
  
  
  s.subspec 'OSX' do |ss|
    
    
    # Platform
    #ss.platform = :osx - Don't put here! It is considered whole pod.
    

    # Dependencies
    ss.dependency s.name + '/NABDefines'
    ss.dependency s.name + '/NABLog'
    ss.dependency s.name + '/NABFlickr'


    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'


  end



  # ----------- Finalized Specs ------------



  s.subspec 'NABLog' do |ss|

    # Dependencies
    ss.dependency 'CocoaLumberjack'

    ss.dependency s.name + '/NABDefines'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end



  s.subspec 'NABDefines' do |ss|

    # Frameworks
    ss.frameworks =
      'Foundation',
      'CoreGraphics'

    ss.osx.frameworks =
      'AppKit'

    ss.ios.frameworks =
      'UIKit'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end



  s.subspec 'NABFlickr' do |ss|

    # Dependencies
    ss.dependency s.name + '/NABLog'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'

    # Subspecs
    ss.subspec 'ObjectiveFlickr' do |sss|

      # Technicals
      sss.requires_arc = false

      # Frameworks
      sss.frameworks =
        'CFNetwork',
        'CoreFoundation',
        'SystemConfiguration'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end

  end
  
  
  
  s.subspec 'FBAudienceNetwork+NABFixCrashes' do |ss|

    # Dependencies
    ss.dependency 'FBAudienceNetwork'
    ss.dependency 'JRSwizzle'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'

  end
  
  
  
  s.subspec 'HockeySDK+NAB' do |ss|
    
    # Frameworks
    ss.frameworks =
      'Foundation'

    # Dependencies
    ss.dependency 'HockeySDK'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'

  end
  
  
  
end