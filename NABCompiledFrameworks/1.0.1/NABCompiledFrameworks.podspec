Pod::Spec.new do |s|

  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}

  s.name     = 'NABCompiledFrameworks'
  s.version  = '1.0.1'
  s.license  = 'Apache License, Version 2.0'
  s.homepage = 'https://github.com/notabasement/ObjC-NABCompiledFrameworks'
  s.summary  = 'Objective-C compiled frameworks for NAB\'s iOS apps.'
  
  s.platform = :ios
  s.ios.deployment_target = "7.0"
  
  
  
  s.ios.frameworks  =
    'AssetsLibrary',
    'CFNetwork',
    'CoreData',
    'CoreGraphics',
    'CoreLocation',
    'CoreMedia',
    'Foundation',
    'ImageIO',
    'QuartzCore',
    'SystemConfiguration',
    'UIKit'
    
  s.ios.prefix_header_contents =
    '@import AssetsLibrary;',
    '@import CFNetwork;', 
    '@import CoreData;',
    '@import CoreGraphics;',
    '@import CoreLocation;',
    '@import CoreMedia;',
    '@import Foundation;',
    '@import ImageIO;',
    '@import QuartzCore;',
    '@import SystemConfiguration;',
    '@import UIKit;',
    '#import "NAB.h"'



  s.ios.dependency 'Facebook-iOS-SDK'
  s.ios.dependency 'instagram-ios-sdk'
  s.ios.dependency 'JSONKit-NoWarning'
  s.ios.dependency 'OAuthCore'
  s.ios.dependency 'Parse'
  s.ios.dependency 'Reachability'
  
  s.ios.xcconfig = {'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Parse'}

  
  
  s.source       = {:git => 'https://github.com/notabasement/ObjC-NABCompiledFrameworks.git', :tag => '1.0.1'}
  s.source_files = 'Source/*.{h,m}', 'Source/**/*.{h,m}'

  
  
  s.requires_arc = true
  
  non_arc_files =
    'Source/ObjectiveFlickr/ObjectiveFlickr.{h,m}',
    'Source/ObjectiveFlickr/OFUtilities.{h,m}',
    'Source/ObjectiveFlickr/OFXMLMapper.{h,m}',
    'Source/ObjectiveFlickr/LFWebAPIKit/LFWebAPIKit.h',
    'Source/ObjectiveFlickr/LFWebAPIKit/LFHTTPRequest.{h,m}',
    'Source/ObjectiveFlickr/LFWebAPIKit/LFSiteReachability.{h,m}',
    'Source/ObjectiveFlickr/LFWebAPIKit/NSData+LFHTTPFormExtensions.{h,m}',
    'Source/GusUtils/NSMutableDictionary+ImageMetadata.{h,m}',
    'Source/NSAttributedString+Attributes/NSAttributedString+Attributes.{h,m}'
    
  s.exclude_files = non_arc_files
  
  s.subspec 'no-arc' do |sna|
    sna.requires_arc = false
    sna.source_files = non_arc_files
    sna.prefix_header_contents =
      '@import AssetsLibrary;',
      '@import CFNetwork;', 
      '@import CoreData;',
      '@import CoreGraphics;',
      '@import CoreLocation;',
      '@import CoreMedia;',
      '@import Foundation;',
      '@import ImageIO;',
      '@import QuartzCore;',
      '@import SystemConfiguration;',
      '@import UIKit;'
  end

end
