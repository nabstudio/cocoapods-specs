Pod::Spec.new do |s|
  
  
  # Set Up Variables
  name       = 'NABFuzelRenderSDK'
  version    = '0.0.57'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCFUZELRENDERSDK/objc-fuzel-render-sdk.git'
  source_dir = 'Source'
  s.source   = {:git => git_url, :tag => version} # Comment/Uncomment this for development/distribution mode.


  # Main Spec - Information
  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'SDK for rendering Fuzel & art assets.'
  s.homepage = git_url
  
  
  # Main Spec - Technicals
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.8'
  
  s.requires_arc = true
  s.xcconfig = {
    'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
  }
  
  
  # Cocoapods bugs:
  # - s.ios.default_subspec and osx doesn't work.
  # - s.osx.dependency also includes ios stuffs
  s.default_subspec = 'iOS/Core'
  
  
  
  # ----------- Specs ------------
  
  
  
  s.subspec 'iOS' do |ss|
    
    
    # Platform
    ss.platform = :ios
    
    
    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'
      
      
    # Subspecs
    ss.subspec 'Core' do |sss|
      
      # Frameworks
      sss.ios.frameworks =
        'AssetsLibrary', # MHTFuzel
        'AVFoundation', # MHTFuzel, MHTFuzel+RenderingWithCache
        'CoreData', # MHTAssetItem
        'CoreImage', # MHTFilter, MHTImageImportManager
        'CoreText', # MHTFuzel
        'MobileCoreServices' # MHTFuzel
      
      # Dependencies
      sss.dependency 'libextobjc'
      sss.dependency 'GPUImage' # MHTFilter
      sss.dependency 'JSONKit-NoWarning' # MHTFuzel, MHTImageImportManager
      sss.dependency 'Mantle' # MHTStyle, MHTTemplate, MHTTool
      sss.dependency 'ZipKit' # MHTAssetItem
      sss.ios.dependency 'NABCompiledFrameworks/iOS/Core'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTAspectRatio/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTAspectRatio/Source/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTAssetItem/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTAssetItem/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFilter/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFilter/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFontManager/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFontManager/Source/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTStyle/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTStyle/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTTemplate/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTTemplate/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTTool/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTTool/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTVideoEffect/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTVideoEffect/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTFuzel/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTFuzel/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTFuzel+RenderingWithCache/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTFuzel+RenderingWithCache/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTImageImportManager/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTImageImportManager/**/*.{h,m}'
        
      # Resources
      sss.resources =
        source_dir + '/' + sss.name + '/MHTAssets/MHTAspectRatio/Resources/*',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFontManager/Resources/*'

    end
    
    ss.subspec 'MHTFuzelView' do |sss|
      
      # Frameworks
      sss.ios.frameworks =
        'CoreText',
        'QuartzCore',
        'UIKit'
      
      # Dependencies
      sss.dependency 'libextobjc'
      sss.ios.dependency 'OHAttributedStringAdditions' # MHTFuzelView
      sss.ios.dependency 'NABCompiledFrameworks/iOS/Core'
      sss.ios.dependency ss.name + '/Core'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/Source/**/*.{h,m}'
        
      # Resources
      sss.resources =
        source_dir + '/' + sss.name + '/Resources/*'

    end
    
    ss.subspec 'MHTFuzelMadison' do |sss|
      
      # Dependencies
      sss.ios.dependency ss.name + '/Core'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end
      
    ss.subspec 'MHTPhotoPicker' do |sss|
      
      # Frameworks
      sss.ios.frameworks =
        'Photos',
        'UIKit'
      
      # Dependencies
      sss.dependency 'libextobjc'
      sss.dependency 'QZConstraints'
      sss.dependency 'ProgressHUD'
      sss.ios.dependency 'NABCompiledFrameworks/iOS/Core'
      sss.ios.dependency 'Parse'
      sss.ios.dependency 'ParseFacebookUtils'
      sss.ios.dependency ss.name + '/Core'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/Source/**/*.{h,m}'
        
      # Resources
      sss.resources =
        source_dir + '/' + sss.name + '/Resources/*'

    end
      
  end
  
  
  
  s.subspec 'OSX' do |ss|
    
    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'

  end
  
end