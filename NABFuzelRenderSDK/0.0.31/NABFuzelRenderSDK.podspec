Pod::Spec.new do |s|
  
  
  # Set Up Variables
  name       = 'NABFuzelRenderSDK'
  version    = '0.0.31'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCFUZELRENDERSDK/objc-fuzel-render-sdk.git'
  source_dir = 'Source'


  # Main Spec - Information
  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'SDK for rendering Fuzel & art assets.'
  s.homepage = git_url
  
  
  # Main Spec - Technicals
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.8'
  
  s.requires_arc = true
  s.xcconfig = {
    'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
  }
  
  s.source   = {:git => git_url, :tag => version} # Comment/Uncomment this for development/distribution mode.
  
  s.header_dir          = name
  s.header_mappings_dir = source_dir
  
  
  # Cocoapods bugs:
  # - s.ios.default_subspec and osx doesn't work.
  # - s.osx.dependency also includes ios stuffs
  s.default_subspec = 'iOS'
  
  
  
  # ----------- Specs ------------
  
  
  
  s.subspec 'iOS' do |ss|
    
    ss.platform = :ios
    
    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'
      
      
    # Subspecs
    ss.subspec 'Core' do |sss|
      
      # Frameworks
      sss.ios.frameworks =
        'AssetsLibrary', # MHTFuzel
        'AVFoundation', # MHTFuzel, MHTFuzel+RenderingWithCache
        'CoreData', # MHTAssetItem
        'CoreImage', # MHTFilter, MHTImageImportManager
        'CoreText', # MHTFuzel
        'MobileCoreServices' # MHTFuzel
      
      # Dependencies
      sss.dependency 'libextobjc'
      sss.dependency 'GPUImage' # MHTFilter
      sss.dependency 'JSONKit-NoWarning' # MHTFuzel, MHTImageImportManager
      sss.dependency 'Mantle' # MHTStyle, MHTTemplate, MHTTool
      sss.dependency 'ZipKit' # MHTAssetItem
      sss.ios.dependency 'NABCompiledFrameworks/iOS'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTAspectRatio/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTAspectRatio/Source/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTAssetItem/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTAssetItem/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFilter/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFilter/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFontManager/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFontManager/Source/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTStyle/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTStyle/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTTemplate/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTTemplate/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTTool/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTTool/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTVideoEffect/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTAssets/MHTVideoEffect/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTFuzel/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTFuzel/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTFuzel+RenderingWithCache/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTFuzel+RenderingWithCache/**/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTImageImportManager/*.{h,m}',
        source_dir + '/' + sss.name + '/MHTImageImportManager/**/*.{h,m}'
        
      # Resources
      sss.resources =
        source_dir + '/' + sss.name + '/MHTAssets/MHTAspectRatio/Resources/*',
        source_dir + '/' + sss.name + '/MHTAssets/MHTFontManager/Resources/*'

    end
    
    ss.subspec 'MHTFuzelView' do |sss|
      
      # Frameworks
      sss.ios.frameworks =
        'CoreText',
        'QuartzCore',
        'UIKit'
      
      # Dependencies
      sss.dependency 'libextobjc'
      sss.ios.dependency 'OHAttributedLabel' # MHTFuzelView
      sss.ios.dependency 'NABCompiledFrameworks/iOS'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/Source/*.{h,m}',
        source_dir + '/' + sss.name + '/Source/**/*.{h,m}'
        
      # Resources
      sss.resources =
        source_dir + '/' + sss.name + '/Resources/*'

    end
      
  end
  
  
  
  s.subspec 'OSX' do |ss|
    
    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'

  end
  
end