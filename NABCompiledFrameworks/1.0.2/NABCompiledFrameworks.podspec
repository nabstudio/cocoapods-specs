Pod::Spec.new do |s|
  
  name       = 'NABCompiledFrameworks'
  version    = '1.0.2'
  git_url    = 'https://github.com/notabasement/ObjC-NABCompiledFrameworks.git'
  source_dir = 'Source'



  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'Objective-C compiled frameworks for NAB\'s iOS apps.'
  s.homepage = git_url
  
  s.platform = :ios
  s.ios.deployment_target = "7.0"
  
  
  
  s.ios.frameworks  =
    'AssetsLibrary',
    'CFNetwork',
    'CoreData',
    'CoreGraphics',
    'CoreLocation',
    'CoreMedia',
    'Foundation',
    'ImageIO',
    'QuartzCore',
    'SystemConfiguration',
    'UIKit'
    
  prefix_header_contents =
    '@import AssetsLibrary;',
    '@import CFNetwork;', 
    '@import CoreData;',
    '@import CoreGraphics;',
    '@import CoreLocation;',
    '@import CoreMedia;',
    '@import Foundation;',
    '@import ImageIO;',
    '@import QuartzCore;',
    '@import SystemConfiguration;',
    '@import UIKit;'
    
  s.ios.prefix_header_contents =
    prefix_header_contents,
    '#import "NAB.h"'



  s.ios.dependency 'Facebook-iOS-SDK'
  s.ios.dependency 'instagram-ios-sdk'
  s.ios.dependency 'JSONKit-NoWarning'
  s.ios.dependency 'OAuthCore'
  s.ios.dependency 'Parse'
  s.ios.dependency 'Reachability'
  
  s.ios.xcconfig = {'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Parse'}

  
  
  s.header_dir   = name
  s.header_mappings_dir = source_dir
  
  s.source       = {:git => git_url, :tag => version}
  s.source_files =
    source_dir,
    source_dir + '/*.{h,m}',
    source_dir + '/**/*.{h,m}'

  
  
  s.requires_arc = true
  
  non_arc_files =
    source_dir + '/ObjectiveFlickr/ObjectiveFlickr.{h,m}',
    source_dir + '/ObjectiveFlickr/OFUtilities.{h,m}',
    source_dir + '/ObjectiveFlickr/OFXMLMapper.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFWebAPIKit.h',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFHTTPRequest.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFSiteReachability.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/NSData+LFHTTPFormExtensions.{h,m}',
    source_dir + '/GusUtils/NSMutableDictionary+ImageMetadata.{h,m}',
    source_dir + '/NSAttributedString+Attributes/NSAttributedString+Attributes.{h,m}'
    
  s.exclude_files = non_arc_files
  
  s.subspec 'no-arc' do |sna|
    sna.requires_arc = false
    sna.source_files = non_arc_files
    sna.prefix_header_contents = prefix_header_contents
  end

end
