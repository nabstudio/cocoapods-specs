Pod::Spec.new do |s|
  
  name       = 'NABCompiledFrameworks'
  version    = '1.0.34'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCCFWK/objc-compiled-frameworks.git'
  source_dir = 'Source'



  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'Objective-C compiled frameworks for NAB\'s iOS apps.'
  s.homepage = git_url
  
  s.platform = :ios
  s.ios.deployment_target = '7.0'
  
  
  
  s.ios.frameworks =
    'AssetsLibrary', # PhotoServices
    'AudioToolbox', # NABSoundManager
    'AVFoundation', # AVAssetExportSession+NAB
    'CFNetwork', # ObjFlickr
    'CoreData', # NABMagicalRecord
    'CoreGraphics',
    'Foundation',
    'MessageUI', # Email
    'QuartzCore',
    'StoreKit', # MKStoreManager
    'SystemConfiguration', # ObjFlickr
    'UIKit'
    
  s.ios.weak_frameworks =
    'Photos' # PhotoServices



  s.ios.dependency 'CocoaLumberjack' # DLog+NAB
  s.ios.dependency 'Facebook-iOS-SDK' # NABFacebookManager
  s.ios.dependency 'GoogleAnalytics-iOS-SDK' # GAI+NAB
  s.ios.dependency 'instagram-ios-sdk' # NABInstagramManager
  s.ios.dependency 'JSONKit-NoWarning' # NABVersionUpdateController
  s.ios.dependency 'libextobjc' # All
  s.ios.dependency 'NSData+Base64' # MKStoreManager, NABNetworkKit
  s.ios.dependency 'Parse' # NABFacebookManager, NABSourceDataObject
  s.ios.dependency 'ParseFacebookUtils' # NABFacebookManager, NABSourceDataObject
  s.ios.dependency 'pop' # NABSnappingCollectionView
  s.ios.dependency 'POP+MCAnimate' # NABSnappingCollectionView
  s.ios.dependency 'RBStoryboardLink' # NABTransition, NABUniversalStoryboardLink
  s.ios.dependency 'Reachability' # UIApplication+NABAppStore, NABNetworkKit
  s.ios.dependency 'SFHFKeychainUtils' # MKStoreManager
  
  s.ios.xcconfig = {
    'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Parse $(PODS_ROOT)/ParseFacebookUtils',
    'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
  }

  
  
  s.header_dir   = name
  s.header_mappings_dir = source_dir
  
  s.source       = {:git => git_url, :tag => version}
  s.source_files =
    source_dir,
    source_dir + '/*.{h,m}',
    source_dir + '/**/*.{h,m}'

  
  
  s.requires_arc = true
  
  non_arc_files =
    source_dir + '/ObjectiveFlickr/ObjectiveFlickr.{h,m}',
    source_dir + '/ObjectiveFlickr/OFUtilities.{h,m}',
    source_dir + '/ObjectiveFlickr/OFXMLMapper.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFWebAPIKit.h',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFHTTPRequest.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/LFSiteReachability.{h,m}',
    source_dir + '/ObjectiveFlickr/LFWebAPIKit/NSData+LFHTTPFormExtensions.{h,m}'
    
  s.exclude_files = non_arc_files
  
  s.subspec 'no-arc' do |sna|
    sna.requires_arc = false
    sna.source_files = non_arc_files
  end

end