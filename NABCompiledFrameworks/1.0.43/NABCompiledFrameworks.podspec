Pod::Spec.new do |s|
  
  # Set Up Variables
  name       = 'NABCompiledFrameworks'
  version    = '1.0.43'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCCFWK/objc-compiled-frameworks.git'
  source_dir = 'Source'


  # Main Spec - Information
  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'Objective-C compiled frameworks for NAB\'s iOS apps.'
  s.homepage = git_url
  
  
  # Main Spec - Technicals
  s.ios.deployment_target = '7.0'
  s.osx.deployment_target = '10.8'
  
  s.requires_arc = true
  s.xcconfig = {
    'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
  }
  
  s.source   = {:git => git_url, :tag => version} # Comment/Uncomment this for development/distribution mode.
  
  s.header_dir          = name
  s.header_mappings_dir = source_dir
  
  
  # Cocoapods bugs:
  # - s.ios.default_subspec and osx doesn't work.
  # - s.osx.dependency also includes ios stuffs
  s.default_subspec = 'iOS'
  
  
  
  # ----------- Specs ------------
  
  
  
  s.subspec 'iOS' do |ss|
    
    # Dependencies
    ss.dependency s.name + '/AAPLFramework'
    ss.dependency s.name + '/NABDefines'
    ss.dependency s.name + '/NABLog'
    ss.dependency s.name + '/NABFlickr'
    
    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'
      
    # Subspecs
    ss.subspec 'Core' do |sss|

      sss.platform = :ios

      # Frameworks
      sss.frameworks =
        'AudioToolbox', # NABSoundManager
        'AVFoundation', # AVAssetExportSession+NAB
        'CoreData', # NABMagicalRecord
        'StoreKit' # MKStoreManager

      sss.ios.frameworks =
        'AssetsLibrary', # PhotoServices
        'MessageUI', # Email
        'UIKit'

      sss.ios.weak_frameworks =
        'Photos' # Only on iOS 8, PhotoServices

      # Dependencies
      sss.dependency 'JSONKit-NoWarning' # NABVersionUpdateController
      sss.dependency 'libextobjc' # All
      sss.dependency 'NSData+Base64' # MKStoreManager, NABNetworkKit
      sss.dependency 'pop' # NABSnappingCollectionView
      sss.dependency 'POP+MCAnimate' # NABSnappingCollectionView
      sss.dependency 'Reachability' # UIApplication+NABAppStore, NABNetworkKit

      sss.dependency 'Facebook-iOS-SDK' # NABFacebookManager
      sss.dependency 'GoogleAnalytics-iOS-SDK' # GAI+NAB
      sss.dependency 'instagram-ios-sdk' # NABInstagramManager
      sss.dependency 'Parse' # NABFacebookManager, NABSourceDataObject
      sss.dependency 'ParseFacebookUtils' # NABFacebookManager, NABSourceDataObject
      sss.dependency 'RBStoryboardLink' # NABTransition, NABUniversalStoryboardLink
      sss.dependency 'SFHFKeychainUtils' # MKStoreManager

      sss.dependency s.name + '/NABDefines'
      sss.dependency s.name + '/NABLog'
      sss.dependency s.name + '/NABFlickr'

      # Technicals
      sss.xcconfig = {
        'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Parse $(PODS_ROOT)/ParseFacebookUtils',
        'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
      }

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end
      
  end
  
  
  
  s.subspec 'OSX' do |ss|

    # Dependencies
    ss.dependency s.name + '/NABDefines'
    ss.dependency s.name + '/NABLog'
    ss.dependency s.name + '/NABFlickr'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'

  end



  # ----------- Finalized Specs ------------



  s.subspec 'NABLog' do |ss|

    # Dependencies
    ss.dependency 'CocoaLumberjack'

    ss.dependency s.name + '/NABDefines'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end



  s.subspec 'NABDefines' do |ss|

    # Frameworks
    ss.frameworks =
      'Foundation',
      'CoreGraphics'

    ss.osx.frameworks =
      'AppKit'

    ss.ios.frameworks =
      'UIKit'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end


  s.subspec 'NABFlickr' do |ss|

    # Dependencies
    ss.dependency s.name + '/NABLog'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}'

    # Subspecs
    ss.subspec 'ObjectiveFlickr' do |sss|

      # Technicals
      sss.requires_arc = false

      # Frameworks
      sss.frameworks =
        'CFNetwork',
        'CoreFoundation',
        'SystemConfiguration'

      # Source
      sss.source_files =
        source_dir + '/' + sss.name + '/*.{h,m}',
        source_dir + '/' + sss.name + '/**/*.{h,m}'

    end

  end
  
  s.subspec 'AAPLFramework' do |ss|

    ss.platform = :ios

    # Dependencies
    ss.dependency s.name + '/NABLog'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/Source/*.{h,m}',
      source_dir + '/' + ss.name + '/Source/**/*.{h,m}'
      
    ss.resources =
      source_dir + '/' + ss.name + '/Resources/*.*'

  end
  
end