Pod::Spec.new do |s|
  
  # Set Up Variables
  name       = 'NABCompiledFrameworks'
  version    = '2.1.0'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCCFWK/objc-compiled-frameworks.git'
  source_dir = 'Pod'
  s.source   = {:git => git_url, :branch => "release/#{version}"}


  # Main Spec - Information
  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'Compiled frameworks for NAB\'s iOS, OSX apps.'
  s.homepage = git_url
  
  
  # Main Spec - Technicals
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.10'
  
  s.requires_arc = true
  
  
  # Cocoapods bugs:
  # - s.ios.default_subspec and osx doesn't work.
  # - s.osx.dependency also includes ios stuffs
  s.default_subspec = 'Dummy'
  
  
  
  # ----------- Subspecs ------------
  
  
  
  s.subspec 'Dummy' do |ss|
    # This is an empty subspec, serves merely as a dummy default subspec,
    # else ALL other subspecs will be installed.
  end
  
  
  
  s.subspec 'iOS' do |ss|
    
    # Dependencies
    ss.dependency s.name + '/NABDefines'
    ss.dependency s.name + '/NABLog'
    ss.dependency s.name + '/NABFlickr'
    ss.dependency s.name + '/NABFastTapGestureRecognizer'
    ss.dependency s.name + '/NABURLProcessor'
    ss.dependency s.name + '/MKStoreManager'
    # ss.dependency s.name + '/AAPLCollectionView2014'
    ss.dependency s.name + '/AAPLCollectionView2015'
    ss.dependency s.name + '/NABTransition'
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/NABFacebook'
    ss.dependency s.name + '/NABUIFeedback'
    ss.dependency s.name + '/UIColor+NABSuggestedColors'
    ss.dependency s.name + '/NABAppLaunchViewController'
    ss.dependency s.name + '/NABSnappingCollectionView'
    ss.dependency s.name + '/NABColorCube'
    ss.dependency s.name + '/NABLocalization'
    ss.dependency s.name + '/NABPopulatingView'
    ss.dependency s.name + '/NABAsyncFileDeleter'
    ss.dependency s.name + '/NABCellRenderer'
    ss.dependency s.name + '/NABForwardDelegate'
    ss.dependency s.name + '/Mantle+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'
    
  end



  s.subspec 'NABDefines' do |ss|

    # Frameworks
    ss.frameworks =
      'Foundation',
      'CoreGraphics'

    ss.osx.frameworks =
      'AppKit'

    ss.ios.frameworks =
      'QuartzCore',
      'UIKit'
      
    # Dependencies
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
    
    
    
  s.subspec 'NABLog' do |ss|

    # Dependencies
    ss.dependency 'CocoaLumberjack'
    ss.dependency s.name + '/NABDefines'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end



  s.subspec 'NABFlickr' do |ss|
    
    # Frameworks
    ss.frameworks =
      'Foundation'

    # Dependencies
    ss.dependency s.name + '/NABLog'
    ss.dependency s.name + '/ObjectiveFlickr'
    
    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'ObjectiveFlickr' do |ss|

    # Technicals
    ss.requires_arc = false

    # Frameworks
    ss.frameworks =
      'CFNetwork',
      'CoreFoundation',
      'SystemConfiguration'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABFastTapGestureRecognizer' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'UIKit'

    # Dependencies
    ss.dependency 'libextobjc'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABURLProcessor' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'
      
    # Dependencies
    ss.dependency s.name + '/NABLog'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'MKStoreManager' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'StoreKit'

    # Dependencies
    ss.dependency 'NSData+Base64'
    ss.dependency 'SFHFKeychainUtils'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  s.subspec 'AAPLCollectionView2014' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'
      
    # Dependencies
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'
      
    # Resources
    ss.resources =
      source_dir + '/' + ss.name + '/Resources/*.*'

  end
  
  
  
  s.subspec 'AAPLCollectionView2015' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'
    
    # Dependencies
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'
      
    # Resources
    ss.resources =
      source_dir + '/' + ss.name + '/Resources/*.*'
      
    # Private
    ss.private_header_files =
      source_dir + '/' + ss.name + '/*_Private.h',
      source_dir + '/' + ss.name + '/**/*_Private.h',
      source_dir + '/' + ss.name + '/*_Internal.h',
      source_dir + '/' + ss.name + '/**/*_Internal.h'

  end
  
  
  
  s.subspec 'NABTransition' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'

    # Dependencies
    ss.dependency 'RBStoryboardLink'
    ss.dependency s.name + '/NABLog'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'Apple+NAB' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'MessageUI',
      'UIKit'

    # Dependencies
    ss.dependency 'Reachability'
    ss.dependency s.name + '/NABLog'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABFacebook' do |ss|
    
    # Platform
    ss.platform = :ios

    # Dependencies
    ss.dependency 'FBSDKCoreKit'
    ss.dependency 'Parse', '>= 1.7.0'
    ss.dependency 'ParseFacebookUtilsV4', '>= 1.7.0'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABUIFeedback' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'

    # Dependencies
    ss.dependency 'pop'
    ss.dependency 'POP+MCAnimate'
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'UIColor+NABSuggestedColors' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'

    # Dependencies
    ss.dependency 'HexColors'
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABAppLaunchViewController' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'

    # Dependencies
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABSnappingCollectionView' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'

    # Dependencies
    ss.dependency 'libextobjc'
    ss.dependency 'pop'
    ss.dependency 'POP+MCAnimate'
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABColorCube' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'CoreImage',
      'UIKit'

    # Dependencies
    ss.dependency 'libextobjc'
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABLocalization' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation'

    # Dependencies
    ss.dependency 'JRSwizzle'
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABPopulatingView' do |ss|
    
    # Platform
    ss.platform = :ios
    
    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'

    # Dependencies
    ss.dependency 'libextobjc'
    ss.dependency 'QZConstraints'
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'
      
    # Resources
    ss.resources =
      source_dir + '/' + ss.name + '/Resources/*.*'

  end
  
  
  
  s.subspec 'NABAsyncFileDeleter' do |ss|

    # Platform
    ss.platform = :ios

    # Frameworks
    ss.frameworks =
      'Foundation'

    # Dependencies
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'NABCellRenderer' do |ss|

    # Platform
    ss.platform = :ios

    # Frameworks
    ss.frameworks =
      'Foundation'

    # Dependencies
    ss.dependency s.name + '/Apple+NAB'
    ss.dependency s.name + '/NABAsyncFileDeleter'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end



  s.subspec 'NABForwardDelegate' do |ss|

    # Platform
    ss.platform = :ios

    # Frameworks
    ss.frameworks =
      'Foundation'
      
    # Dependencies
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'Mantle+NAB' do |ss|

    # Platform
    ss.platform = :ios

    # Frameworks
    ss.frameworks =
      'Foundation'
      
    # Dependencies
    ss.dependency 'Mantle'
    ss.dependency s.name + '/UIKit+NAB'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
  s.subspec 'UIKit+NAB' do |ss|

    # Platform
    ss.platform = :ios

    # Frameworks
    ss.frameworks =
      'Foundation',
      'UIKit'
      
    # Dependencies
    ss.dependency 'JRSwizzle'

    # Source
    ss.source_files =
      source_dir + '/' + ss.name + '/*.{h,m}',
      source_dir + '/' + ss.name + '/**/*.{h,m}'

  end
  
  
  
end