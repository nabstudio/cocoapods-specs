Pod::Spec.new do |s|
  s.name         = 'ProgressHUD'
  s.version      = '2.3.1'
  s.summary      = 'ProgressHUD is a lightweight and easy-to-use HUD for iOS 8. (Objective-C)'
  s.homepage     = 'https://github.com/relatedcode/ProgressHUD'
  s.license      = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author       = 'Related Code'
  s.platform     = :ios, '8.0'
  s.source       = { :git => 'https://github.com/notabasement/ProgressHUD.git', :branch => 'release/' + s.version.to_s }
  s.source_files = 'ProgressHUD/ProgressHUD/ProgressHUD.{h,m}'
  s.resource     = 'ProgressHUD/ProgressHUD/ProgressHUD.bundle'
  s.requires_arc = true
end
