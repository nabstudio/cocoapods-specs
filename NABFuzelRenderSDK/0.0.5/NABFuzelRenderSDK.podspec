Pod::Spec.new do |s|
  
  name       = 'NABFuzelRenderSDK'
  version    = '0.0.5'
  git_url    = 'http://cosmos.notabasement.com/diffusion/OBJCFUZELRENDERSDK/objc-fuzel-render-sdk.git'
  source_dir = 'Source'



  s.name     = name
  s.version  = version
  s.author   = {'Not A Basement Studio' => 'support@notabasement.com'}
  s.license  = 'Apache License, Version 2.0'
  s.summary  = 'SDK for rendering Fuzel & art assets.'
  s.homepage = git_url
  
  s.platform = :ios
  s.ios.deployment_target = "7.0"
  
  
  
  s.ios.frameworks  =
    'AssetsLibrary',
    'AVFoundation',
    'CoreData',
    'CoreGraphics',
    'CoreText',
    'Foundation',
    'MobileCoreServices',
    'QuartzCore',
    'UIKit'
    
    
    
  s.ios.dependency 'NABCompiledFrameworks'
  s.ios.dependency 'GPUImage', '0.1.2'
  s.ios.dependency 'JSONKit-NoWarning'
  s.ios.dependency 'ZipKit'



  s.ios.xcconfig = {
    'GCC_PREPROCESSOR_DEFINITIONS' => 'CONFIGURATION_$CONFIGURATION $(inherited)'
  }

  
  
  s.header_dir   = name
  s.header_mappings_dir = source_dir
  
  s.source       = {:git => git_url, :tag => version}
  s.source_files =
    source_dir,
    source_dir + '/*.{h,m}',
    source_dir + '/**/*.{h,m}'
    
  s.resources =
    source_dir + '/*.plist',
    source_dir + '/**/*.plist'

  
  
  s.requires_arc = true

end
